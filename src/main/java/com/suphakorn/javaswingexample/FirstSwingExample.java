/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.javaswingexample;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author exhau
 */
public class FirstSwingExample {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        
        JButton button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        
        frame.add(button);
        
        frame.setSize(400, 500);
        frame.setLayout(null);
        frame.setVisible(true);
        
    }
}
